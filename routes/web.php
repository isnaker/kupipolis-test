<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

Route::get('/', function () {
    $data = [];
    if (DB::connection()->getDatabaseName()){
        $data = \App\Upload::all();
    }

    return view('upload', [ 'files' => Storage::files('files'), 'data' => $data ]);
})->name('home');


Route::get('/load/{name}', function($name){
    return view('table', ['filename' => $name]);
});

Route::get('/fromdb/{id}', function($id){
    return view('table', ['upload' => \App\Upload::findOrFail($id) ]);
});

/* API calls */

Route::post('/upload', function (Request $request) {

    $request->validate(['upload' => 'required']);

    $path = $request->file('upload')->store('files');

    Excel::load( Storage::path($path), function($reader) use ($request) {

        $results = $reader->toArray();

        foreach ($results as &$result){
            $result = (array_values($result));
        }

        // сохранение данных в базу, если база подключена
        if ( DB::connection()->getDatabaseName() ){
            $upload = new \App\Upload;
            $upload->data = json_encode($results);
            $upload->name = $request->file('upload')->getClientOriginalName();
            $upload->save();
        }

    });

    return redirect(route('home'));
})->name('upload');


Route::get('/get/{name}', function ($name){
    // Эта версия работает с файлами - без привязки к бд
    // В этом режиме не возвращается "красивое" имя загруженного файла и
    // дата его загрузки

    Excel::load( Storage::path('files/'.$name), function($reader) {
        $results = $reader->toArray();

        foreach ($results as &$result){
            $result = (array_values($result));
        }

        print( (json_encode($results)) );
    });
});

Route::get('/db/{id}', function ($id){
    return \App\Upload::findOrFail($id)->data;
});
