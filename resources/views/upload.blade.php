<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <title>Главная</title>

</head>
<body>
<div class="container full-height">
    <div class="content">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <section class="jumbotron text-center mt-4">
                <div class="container">
                    <h1 class="jumbotron-heading">Загрузка файла</h1>
                    <p class="lead text-muted">Принимаются форматы: XLS и XLSX</p>

                    <div>
                        <form method="post" action="{{ route('upload') }}"  enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input name="upload" accept=".xlsx, application/vnd.sealed-xls" type="file">
                            <button class="btn btn-primary my-2" type="submit">Загрузить</button>
                        </form>
                    </div>

                </div>
            </section>

        <div class="row">

            <div class="col">
                <h4>Загрузки (файловая система)</h4>

                <ul class="list-group">
                    @foreach($files as $file)
                        <li class="list-group-item"><a href="/load/{{ basename($file) }}">{{ $file }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="col">
                <h4>Загрузки (База данных)</h4>

                <ul class="list-group">
                    @foreach($data as $item)
                        <li class="list-group-item"><a href="/fromdb/{{ $item->id }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>

        </div>


    </div>
</div>
</body>
</html>


