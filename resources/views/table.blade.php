<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <title>Загруженный файл</title>

    <script src="{{ mix('js/app.js') }}"></script>
</head>

<body>

<div class="container">

    <a class="btn btn-primary mt-2" href="{{ route('home') }}">< Назад</a>
    <hr>

    @if (isset($filename))
        <h5>Файл: <span id="filename">{{ $filename }}</span></h5>
    @endif

    @if (isset($upload))
        <h5>Файл: {{ $upload->name }}. ( id = <small id="id">{{ $upload->id }}</small>) </h5>

    @endif

    <div id="app">
        <app></app>
    </div>
</div>

</body>



